package com.company.Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBconnection {
    private Connection dbConnection;
    private String url, user, password;

    public DBconnection() throws SQLException {
        //DB en local
        /*url = "jdbc:mysql://localhost:3306/contactDB?useUnicode=true&useJDBCCompliantTimezoneShift=" +
                "true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        user ="root";
        password = "";*/

        //DB en ligne
        url="jdbc:mysql://mysql-database.alwaysdata.net/database_contactdb?useUnicode=true&useJDBCCompliantTimezoneShift=" +
                "true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        user="database_halima";
        password="database::halima";

        dbConnection = DriverManager.getConnection(url, user, password);
        System.out.println("connecté");


    }

    public Connection getDbConnection() {
        return dbConnection;
    }

    public void disconnect() throws SQLException{
        dbConnection.close();
    }

    //CREATE DATABASE contactDB
    //CREATE TABLE contacts (id INT NOT NULL AUTO_INCREMENT, lastname VARCHAR(50) NOT NULL,
    //   firstname VARCHAR(30) NOT NULL, phoneNumber  VARCHAR(50) NOT NULL, PRIMARY KEY (id));
}
