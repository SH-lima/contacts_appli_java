package com.company.Views;

import com.company.Data.Contact;
import com.company.Models.ContactTable;
import com.company.Views.Components.Button;
import com.company.Views.Components.FormFrame;
import com.company.Views.Components.Label;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class EditForm extends FormFrame implements ActionListener {
    private JButton delete_btn, update_btn;
    private Contact_Appli mainFrame;
    private Contact contact = null;
    private ContactTable contact_tb;

    public EditForm(int id, Contact_Appli mainJF){
        super();
        //Frame principal
        mainFrame = mainJF;

        //header
        header_lbl = new Label("Edition du Contact", sizeLbl, mainPanel, BorderLayout.NORTH );

        //center
            try {
                contact_tb = new ContactTable();
                //get contact
                contact = contact_tb.getcontactById(id);
            } catch (SQLException e) {
                //Alert error
                JOptionPane.showMessageDialog(this,e.getMessage(),
                        "", JOptionPane.INFORMATION_MESSAGE);
            }
        if (contact != null) {
            firstnm_tf.setText(contact.getFirstname());
            lastnm_tf.setText(contact.getLastname());
            phone_nmb_tf.setText(contact.getPhoneNumber());
        }


        //footer
        delete_btn = new Button("Supprimer", this, footerPanel);
        update_btn = new Button("Mettre à jour", this, footerPanel);



    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int res;
        //modifier un contact
        if(e.getSource() == update_btn){
            try {
                //vérifier que tous les champs ne sont pas vide
                if(!firstnm_tf.getText().trim().isEmpty()
                        && !lastnm_tf.getText().trim().isEmpty()
                        && !phone_nmb_tf.getText().trim().isEmpty()) {
                    contact.setFirstname(firstnm_tf.getText());
                    contact.setLastname(lastnm_tf.getText());
                    contact.setPhoneNumber(phone_nmb_tf.getText());
                    // appeler la méthode update de DB
                    res = contact_tb.updateContact(contact);
                    // contact modifié
                    if(res == 1) {
                        //rafraichir mainframe avec la nouvelle data et fermer Edit frame
                        mainFrame.dispose();
                        mainFrame = new Contact_Appli();
                        this.dispose();

                        // contact n'a pas été modifié
                    }else{
                        JOptionPane.showMessageDialog(this,"le contact n'a pas été modifié",
                                "", JOptionPane.WARNING_MESSAGE);
                    }
                }else{
                    JOptionPane.showMessageDialog(this,"Il faut remplir tous les champs",
                            "", JOptionPane.WARNING_MESSAGE);
                }

            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this,ex.getMessage(),
                        "", JOptionPane.WARNING_MESSAGE);
            }
        }
        //supprimer un contact
        else if(e.getSource() == delete_btn){
            try {
                res = contact_tb.deleteContact(contact);
                //contact a été supprimé
                if(res == 1) {
                    //rafraichir mainframe avec la nouvelle data et fermer Edit frame
                    mainFrame.dispose();
                    mainFrame = new Contact_Appli();
                    this.dispose();

                //contact n'a pas été supprimé
                }else{
                    JOptionPane.showMessageDialog(this,"le contact n'a pas été supprimé",
                            "", JOptionPane.WARNING_MESSAGE);
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this,ex.getMessage(),
                        "", JOptionPane.WARNING_MESSAGE);
            }
        }


    }
}
