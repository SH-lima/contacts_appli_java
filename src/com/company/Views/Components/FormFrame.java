package com.company.Views.Components;

import javax.swing.*;
import java.awt.*;

public class FormFrame extends JFrame{
    protected JPanel mainPanel, centerPanel, footerPanel;
    protected JTextField firstnm_tf, lastnm_tf ,phone_nmb_tf;
    protected JLabel header_lbl, firstnm_lbl,lastnm_lbl, phone_nmb_lbl;
    protected Dimension sizeLbl;

    public FormFrame(){
        super();
        mainPanel = new JPanel(new BorderLayout());
        centerPanel = new JPanel(new GridLayout(3,2));
        footerPanel = new JPanel();
        sizeLbl = new Dimension(120, 30);

        //center
        firstnm_lbl = new Label("Prénom : ", SwingConstants.RIGHT, centerPanel);
        firstnm_tf = new TextField(centerPanel);
        lastnm_lbl = new Label("Nom : ", SwingConstants.RIGHT, centerPanel);
        lastnm_tf = new TextField(centerPanel);
        phone_nmb_lbl = new Label("Téléphone : ", SwingConstants.RIGHT , centerPanel);
        phone_nmb_tf = new TextField(centerPanel);


        //main
        mainPanel.add(centerPanel, BorderLayout.CENTER);
        mainPanel.add(footerPanel, BorderLayout.SOUTH);


        //Config de frame
        setSize(250,200);
        setBackground(Color.CYAN);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setContentPane(mainPanel);
        setVisible(true);
    }
}
