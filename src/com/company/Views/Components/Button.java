package com.company.Views.Components;

import javax.swing.*;
import java.awt.event.ActionListener;

public class Button extends JButton {

    public Button(String text, ActionListener actionListener, JPanel panel){
        super(text);
        addActionListener(actionListener);
        panel.add(this);
    }

}
