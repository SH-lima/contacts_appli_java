package com.company.Views.Components;

import javax.swing.*;
import java.awt.*;

public class TextField extends JTextField {

    public TextField(JPanel panel){
        super();
        panel.add(this);
    }

    public TextField(Dimension size, JPanel panel){
        super();
        panel.add(this);
        setPreferredSize(size);
    }
}
