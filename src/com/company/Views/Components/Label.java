package com.company.Views.Components;

import javax.swing.*;
import java.awt.*;

public class Label extends JLabel {

    public Label(String text, JPanel panel){
        super(text,  SwingConstants.CENTER);
        panel.add(this);
    }

    public Label(String text, int horizontalAlignment , JPanel panel){
        super(text,  horizontalAlignment);
        panel.add(this);
    }

    public Label(String text, JPanel panel, Dimension size){
        super(text,  SwingConstants.CENTER);
        panel.add(this);
        setPreferredSize(size);
    }

    public Label(String text, Dimension size, JPanel panel, Object constraints){
        super(text,  SwingConstants.CENTER);
        panel.add(this, constraints);
        setPreferredSize(size);
    }

}
