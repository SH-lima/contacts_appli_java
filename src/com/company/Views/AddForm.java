package com.company.Views;

import com.company.Data.Contact;
import com.company.Models.ContactTable;
import com.company.Views.Components.Button;
import com.company.Views.Components.FormFrame;
import com.company.Views.Components.Label;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class AddForm extends FormFrame implements ActionListener {
    private Contact_Appli mainFrame;

    public AddForm(Contact_Appli mainJF){
        super();
        //Frame principal
        this.mainFrame = mainJF;

        //header
        header_lbl = new Label("Nouveau Contact", sizeLbl, mainPanel, BorderLayout.NORTH );

        //footer
        new Button("Créer contact", this, footerPanel);


    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int res;
        ContactTable contact_tb = null;
        try {
            contact_tb = new ContactTable();
            if(!firstnm_tf.getText().trim().isEmpty()
                    && !lastnm_tf.getText().trim().isEmpty()
                    && !phone_nmb_tf.getText().trim().isEmpty()) {

                res = contact_tb.addContact(
                        new Contact(firstnm_tf.getText(), lastnm_tf.getText(), phone_nmb_tf.getText())
                );
                if (res == 1) {
                    //rafraichir mainframe avec la nouvelle data et fermer Edit frame
                    mainFrame.dispose();
                    mainFrame = new Contact_Appli();
                    this.dispose();
                    //
                }
            }
            else{
                JOptionPane.showMessageDialog(this, "il faut remplir tous les champs",
                        "", JOptionPane.WARNING_MESSAGE);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    "", JOptionPane.WARNING_MESSAGE);
        }
    }

}
