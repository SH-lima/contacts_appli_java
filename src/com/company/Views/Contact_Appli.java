package com.company.Views;

import com.company.Data.Contact;
import com.company.Models.ContactTable;
import com.company.Views.Components.Button;
import com.company.Views.Components.Label;
import com.company.Views.Components.TextField;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

public class Contact_Appli extends JFrame implements ActionListener {

    private  JPanel mainPanel, headerPanel, centerPanel, footerPanel;
    private JTextField id_tf;
    private JButton edit_btn, newContact_btn;
    private JTextArea contacts_tf;


    public Contact_Appli(){
        super();
        Dimension sizeItem = new Dimension(120, 30);
        mainPanel = new JPanel(new BorderLayout());
        headerPanel = new JPanel();
        centerPanel = new JPanel(new BorderLayout());
        footerPanel = new JPanel();

        //header
        new Label("id : ", headerPanel ,sizeItem );
        id_tf = new TextField(sizeItem, headerPanel);
        edit_btn = new Button("Editer",this,headerPanel );


        //center
        //contactsList ="";//
        contacts_tf = new JTextArea(getContacListString());
        contacts_tf.setEditable(false);
        centerPanel.add(contacts_tf);

        //footer
        newContact_btn = new Button("Nouveau Contact",this , footerPanel);

        //main
        mainPanel.add(headerPanel, BorderLayout.NORTH);
        mainPanel.add(centerPanel, BorderLayout.CENTER);
        mainPanel.add(footerPanel, BorderLayout.SOUTH);


        //Config de frame
        setSize(350,250);
        setBackground(Color.CYAN);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setContentPane(mainPanel);
        setVisible(true);
    }

    public String getContacListString(){
         ContactTable contact_tb = null;
         String contactsList ="";
         try {
             contact_tb = new ContactTable();
             ArrayList<Contact> arr = contact_tb.read();
             if(arr.isEmpty()){
                 contactsList = "   Aucun contact";
             }else {
                 for (Contact item : arr) {
                     contactsList += " " + item.getId() + " | " + item.getLastname() + " | "
                             + item.getFirstname() + "\n";
                 }
             }

         } catch (SQLException e) {
             e.printStackTrace();
             contactsList = "Error";
         }
         return contactsList;
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        int id;
        //modifier un contact
       if (e.getSource() == edit_btn) {
           if(id_tf.getText().trim().isEmpty())
               JOptionPane.showMessageDialog(this,
                       "Attention! il faut ajouter un chiffre dans le champs",
                       "", JOptionPane.WARNING_MESSAGE);
           else {
               try{
                   id = Integer.parseInt(id_tf.getText().trim());
                   new EditForm(id, this);

               }catch (NumberFormatException nfe){
                   JOptionPane.showMessageDialog(this,
                           "Attention! il faut mettre un chiffre dans le champs",
                           "", JOptionPane.WARNING_MESSAGE);
               }
               id_tf.setText("");
           }
       }
       //ajouter un contact
        else if(e.getSource() == newContact_btn){
            new AddForm(this);
       }

    }
}
