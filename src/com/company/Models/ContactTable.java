package com.company.Models;

import com.company.Data.Contact;
import com.company.Database.DBconnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ContactTable  {
    private String query;
    private PreparedStatement ps;
    private DBconnection db;

    public ContactTable() throws SQLException {
        db = new DBconnection();
    }

    public ArrayList<Contact> read() throws SQLException {
        ArrayList<Contact> contacts = new ArrayList<>();
        Contact contact = null;
        query = "SELECT * FROM contacts";

        ps = db.getDbConnection().prepareStatement(query);
        ResultSet rs = ps.executeQuery();

        while(rs.next()){
            contact = new Contact();
            contact.setId(rs.getInt("id"));
            contact.setFirstname(rs.getString("firstname"));
            contact.setLastname(rs.getString("lastname"));
            contact.setPhoneNumber(rs.getString("phoneNumber"));
            contacts.add(contact);
        }

        rs.close();
        ps.close();
        return contacts;
    }

    public Contact getcontactById(int id) throws SQLException{
        Contact contact = null;
        query = "SELECT * FROM contacts WHERE id=?";
        ps = db.getDbConnection().prepareStatement(query);
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();

        if(rs.next()){
            contact = new Contact();
            contact.setId(rs.getInt("id"));
            contact.setFirstname(rs.getString("firstname"));
            contact.setLastname(rs.getString("lastname"));
            contact.setPhoneNumber(rs.getString("phoneNumber"));
        }
        rs.close();
        ps.close();
        return contact;
    }

    public Contact getcontactByName(Contact contact) throws SQLException{

        query = "SELECT * FROM contacts WHERE firstname=? and lastname=?";
        ps = db.getDbConnection().prepareStatement(query);
        ps.setString(1, contact.getFirstname());
        ps.setString(2, contact.getLastname());
        ResultSet rs = ps.executeQuery();

        if(rs.next()){
            contact.setId(rs.getInt("id"));
            contact.setFirstname(rs.getString("firstname"));
            contact.setLastname(rs.getString("lastname"));
            contact.setPhoneNumber(rs.getString("phoneNumber"));
        }
        rs.close();
        ps.close();
        return contact;
    }

    public int addContact(Contact contact) throws SQLException{
        String query = "INSERT INTO contacts (firstname, lastname, phoneNumber) VALUES (?, ?, ?)";

        ps = db.getDbConnection().prepareStatement(query);
        ps.setString(1, contact.getFirstname());
        ps.setString(2, contact.getLastname());
        ps.setString(3, contact.getPhoneNumber());

        int row= ps.executeUpdate();

        //ps.close();
        return row;
    }

    public int updateContact(Contact contact) throws SQLException{
        String query = "UPDATE contacts SET firstname=?, lastname=?, phoneNumber=? WHERE id= ? ";

        ps = db.getDbConnection().prepareStatement(query);
        ps.setString(1, contact.getFirstname());
        ps.setString(2, contact.getLastname());
        ps.setString(3, contact.getPhoneNumber());
        ps.setInt(4, contact.getId());

        int row = ps.executeUpdate();

        ps.close();
        return row;
    }

    public int deleteContact(Contact contact) throws SQLException{
        String query = "DELETE FROM contacts  WHERE id= ? ";

        ps = db.getDbConnection().prepareStatement(query);
        ps.setInt(1, contact.getId());

        int row = ps.executeUpdate();

        ps.close();
        return row;
    }



}
